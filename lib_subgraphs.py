#!/usr/bin/env python
# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
# Distributed under GPLv2+;  generated content under CC-BY-SA 3.0
# (to view a copy, visit http://creativecommons.org/licenses/by-sa/3.0/)
"""Subgraph library incl. visual aspects"""

from lib import LibSubgraph
from lib_attributes import *


class SubgraphInvisible(LibSubgraph):
    defaults = dict((
        STYLE.invis,
    ))


class SubgraphImportant(LibSubgraph):
    defaults = dict((
        FILLCOLOR('#eaeaea'),
        STYLE('bold, filled, rounded'),
    ))


class SubgraphStandard(LibSubgraph):
    defaults = dict((
        FILLCOLOR('#f5f5f5'),
        STYLE('filled, rounded'),
    ))
