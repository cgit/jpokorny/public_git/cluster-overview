#!/usr/bin/env python
# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
# Distributed under GPLv2+;  generated content under CC-BY-SA 3.0
# (to view a copy, visit http://creativecommons.org/licenses/by-sa/3.0/)
"""Model of cman-based cluster capturing deployment, communication, etc.

By default, XDot window is displayed showing the model in the interactive
way (free zoom, hints upon click, etc.).

The argument, if provided, denotes the output format that will be used for
headleass static capture of this model.  The output file will be named as
per this script modulo extension.  Generally, you can specify arbitrary
format/extension supported by graphviz as a first parameter and you'll get
what you ask for, including original '.dot' file for further utilization.
"""

from sys import argv
from os.path import splitext
from lib import LibDot, main
from lib_subgraphs import *
from lib_nodes import *  # remove when possible
from lib_edges import *  # ditto
from lib_cman import *
from lib_rhel import *


#FONT = 'Inconsolata'  # yum install levien-inconsolata-fonts
FONT = 'Droid Sans Mono'  # yum install google-droid-sans-mono-fonts
FONTDEF = map(lambda switch: '-' + switch + 'fontname=' + FONT, "GNE")
BLACKLIST = []
#BLACKLIST = [Daemon]
OUTPUT = splitext(__file__)[0]


graph = lambda\
:LibDot('Overview of CMAN-based cluster architecture (~el6)'
,overlap='scalexy'
,compound='true'
,splines='yes'
#,rankdir='LR'
#, ranksep='1.2'
,_subgraphs=\
    [SubgraphInvisible('environment-outer'
    #
    # outer environment (take the tools as examples)
    #
    ,label='outer environment'
    ,_subgraphs=\
        [SubgraphInvisible('cluster.outer-clients'
        ,label='external clients'
        ,_nodes=\
            [Executable('firefox')
            ,RhelSaslauthd('saslauthd')
            ,Executable('snmpwalk')
            ,Executable('wbemcli')
        ])
    ])
    ,SubgraphInvisible('environment-inner'
    #
    # inner environment (proper cluster parts)
    #
    ,label='inner environment'
    ,_subgraphs=\
        [SubgraphInvisible('management'
        #
        # cluster management layer
        #
        ,_label='management'
        ,_subgraphs=\
            [SubgraphStandard('cluster.management-cli'
            #
            # management client
            #
            ,label='management client'
            ,_nodes=\
                [CmanLuci('luci')
                ,CmanCcs('ccs')
                ,CmanCcsSync('ccs_sync')
            ])
        ])
        ,SubgraphInvisible('cluster.nodes'
        #
        # cluster nodes layer
        #
        ,label='nodes'
        ,_subgraphs=\
            [SubgraphImportant('cluster.node_c'
            #
            # node c
            #
            ,label='NODE C'
            ,_subgraphs=\
                [SubgraphStandard('cluster.node_c-conga'
                #
                # node c - management
                #
                ,label='management (incomplete)'
                ,_nodes=\
                    [CmanRicci('node_c-ricci'
                    )
                    ,CmanModclusterd('node_c-modclusterd'
                    )]
                )]
            ,_nodes=\
                [Program('node_c-kernel'
                ,label='kernel'
                )
                ,CmanRGManager('node_c-rgmanager'
                )]
            )
            ,SubgraphImportant('cluster.node_b'
            #
            # node b (in detail)
            #
            ,label='NODE B (detailed)'
            ,_subgraphs=\
                [SubgraphStandard('cluster.node_b-conga'
                #
                # node b - management
                #
                ,label='management'
                ,_nodes=\
                    [Agent('node_b-libClusterMonitorSnmp.so'
                    ,label='libClusterMonitor\nSnmp.so'
                    )
                    ,Agent('node_b-libRedHatClusterProvider.so'
                    ,label='libRedHatCluster\nProvider.so'
                    )
                    ,UnixSocket('node_b-clumond.sock'
                    ,label='/var/run/\nclumond.sock'
                    )
                    ,CmanRicci('node_b-ricci'
                    )
                    ,CmanRicciWorker('node_b-ricci-worker'
                    )
                    ,CmanCmanTool('node_b-cman_tool'
                    )
                    ,CmanFenceTool('node_b-fence_tool'
                    )
                    ,CmanModclusterd('node_b-modclusterd'
                    )
                    ,CmanModcluster('node_b-modcluster'
                    )
                    ,Executable('node_b-ricci-modules'
                    ,label='ricci-mod{log,\nrpm,service,\nstorage,virt}'
                    )],
                _edges=\
                    [Consume\
                    ('node_b-libClusterMonitorSnmp.so'
                    ,'node_b-clumond.sock'
                    )
                    ,Consume\
                    ('node_b-libRedHatClusterProvider.so'
                    ,'node_b-clumond.sock'
                    )
                    ,Consume\
                    ('node_b-modcluster'
                    ,'node_b-clumond.sock'
                    )
                    ,Produce\
                    ('node_b-modclusterd'
                    ,'node_b-clumond.sock'
                    )]
                )
                ,SubgraphStandard('cluster.node_b-core'
                #
                # node b - core
                #
                ,label='core'
                ,_nodes=\
                    [SharedCorosync('node_b-corosync'
                    )
                    ,Library('node_b-service_cman'
                    ,label='service_\ncman.lcrso'
                    )
                    ,CmanFenced('node_b-fenced'
                    )
                    ,CmanRGManager('node_b-rgmanager'
                    )
                    ,Library('node_b-libdlm'
                    ,label='libdlm'
                    )
                    ,CmanClustat('node_b-clustat'
                    )
                    ,CmanQdiskd('node_b-qdiskd'
                    )
                    ,UnixSocket('node_b-cman_client'
                    ,label='/var/run/\ncman_client'
                    )
                    ,UnixSocket('node_b-rgmanager.sk'
                    ,label='/var/run/\ncluster/\nrgmanager.sk'
                    )]
                ,_edges=\
                    [Delegate\
                    ('node_b-corosync'
                    ,'node_b-service_cman'
                    )
                    ,Consume\
                    ('node_b-clustat'
                    ,'node_b-cman_client'
                    )
                    ,Consume\
                    ('node_b-clustat'
                    ,'node_b-rgmanager.sk'
                    )
                    ,Consume\
                    ('node_b-fenced'
                    ,'node_b-cman_client'
                    )
                    ,Consume\
                    ('node_b-rgmanager'
                    ,'node_b-cman_client'
                    )
                    ,Consume\
                    ('node_b-rgmanager'
                    ,'node_b-libdlm'
                    )
                    ,Produce\
                    ('node_b-service_cman'
                    ,'node_b-cman_client'
                    ,constraint=False
                    )
                    ,Produce\
                    ('node_b-rgmanager'
                    ,'node_b-rgmanager.sk'
                    )]
                )
                ,SubgraphStandard('cluster.node_b-config'
                #
                # node b - config
                #
                ,label='config'
                ,_nodes=\
                    [Artefact('node_b-cluster.conf'
                    ,label='/etc/cluster/\ncluster.conf'
                    )
                    ,Library('node_b-libconfdb'
                    ,label='libconfdb'
                    )]
                ,_edges=\
                    [Consume\
                    ('node_b-libconfdb'
                    ,'node_b-cluster.conf'
                    ,label='version?'
                    )]
                )
                ,SubgraphStandard('cluster.node_b-fence-agents'
                #
                # node b - fence agents
                #
                ,label='fence agents'
                ,_nodes=\
                    [SharedFenceVirt('node_b-fence_virt'
                    ,label='virt\,xvm'
                    )
                    ,Executable('node_b-fence_others'
                    ,label='others'
                    )
                    ,Executable('node_b-fence_scsi-sanlock'
                    ,label='scsi\,sanlock'
                    )]
                )
                ,SubgraphStandard('cluster.node_b-resource-agents'
                #
                # node b - resource agents
                #
                ,label='resource agents (/usr/share/cluster/*)'
                ,_subgraphs=\
                    [SubgraphStandard('cluster.node_b-resources-aux'
                        ,label='auxiliary'
                        ,_nodes=\
                            [Program('ip.sh'
                            )
                            ,Program('fs.sh'
                            )
                            ,Program('node_b-resources-aux-others'
                            ,label='…'
                            )]
                        ,_edges=\
                            [EdgeInvisible\
                            ('ip.sh'
                            ,'fs.sh'
                            )
                            ,EdgeInvisible
                            ('ip.sh'
                            ,'node_b-resources-aux-others'
                            )]
                    )
                    ,SubgraphStandard('cluster.node_b-resources-native'
                        ,label='native'
                        ,_nodes=\
                            [Program('apache.sh'
                            )
                            ,Program('node_b-resources-native-others'
                            ,label='…'
                            )]
                        ,_edges=\
                            [EdgeInvisible\
                            ('apache.sh'
                            ,'node_b-resources-native-others'
                            )]
                    )]
                ,_nodes=\
                    [Executable('script.sh'
                    )]
                )
                ,SubgraphStandard('cluster.node_b-installed-services'
                #
                # node b - installed services
                #
                ,label='installed services'
                ,_nodes=\
                    [Executable('httpd'
                    ,label='Apache\nHTTP server'
                    )
                    ,Executable('other-services'
                    ,label='…'
                    )]
                )
                ,SubgraphStandard('cluster.node_b-initscripts'
                #
                # node b - initscripts
                #
                ,label='initscripts'
                ,_nodes=\
                    [Executable('other-initscripts'
                    ,label='…'
                    )]
                )
                ,SubgraphStandard('cluster.node_b-kernel'
                #
                # node b - kernel
                #
                ,label='kernel'
                ,_nodes=\
                    [Program('node_b-io'
                    ,label='I/O'
                    )
                    ,Program('node_b-dlm'
                    ,label='dlm'
                    )
                    ,Artefact('node_b-configfs_dlm'
                    ,label='configfs/dlm/'
                    )]
                ,_edges=\
                    [Produce\
                    ('node_b-dlm'
                    ,'node_b-configfs_dlm'
                    )]
                )]
            ,_nodes=\
                [RhelSnmpd('node_b-snmpd'
                )
                ,RhelCimserver('node_b-cimserver'
                )
                ,RhelSaslauthd('node_b-saslauthd'
                )
                ,Artefact('node_b-sys_kernel_config_dlm'
                ,label='/sys/kernel/\nconfig/dlm/'
                )
                ,Artefact('node_b-dev_dlm'
                ,label='/dev/dlm*'
                )]
            ,_edges=\
                [Consume\
                ('node_b-rgmanager'
                ,'node_b-libconfdb'
                )
                ,Consume\
                ('node_b-qdiskd'
                ,'node_b-libconfdb'
                )
                ,Consume\
                ('node_b-fenced'
                ,'node_b-libconfdb'
                )
                ,Consume\
                ('node_b-fence_tool'
                ,'node_b-libconfdb'
                )
                ,Consume\
                ('node_b-modcluster'
                ,'node_b-cluster.conf'
                ,label='version?'
                )
                ,Consume\
                ('node_b-modclusterd'
                ,'node_b-cluster.conf'
                ,label='version?'
                )
                ,Consume\
                ('node_b-modclusterd'
                ,'node_b-cman_client'
                ,constraint=False
                #,label='version?'
                )
                ,Consume\
                ('node_b-modcluster'
                ,'node_b-cman_client'
                ,constraint=False
                )
                ,Consume\
                ('node_b-ricci'
                ,'node_b-cluster.conf'
                ,label='clustername\nclusteralias'
                )
                ,Consume\
                ('node_b-snmpd'
                ,'node_b-libClusterMonitorSnmp.so'
                )
                ,Consume\
                ('node_b-cimserver'
                ,'node_b-libRedHatClusterProvider.so'
                )
                ,Delegate\
                ('node_b-ricci'
                ,'node_b-saslauthd'
                )
                ,Delegate\
                ('node_b-ricci'
                ,'node_b-ricci-worker'
                ,constraint=False
                )
                ,DelegateOddjobExec\
                ('node_b-ricci-worker'
                ,'node_b-modcluster'
                )
                ,DelegateOddjobExec\
                ('node_b-ricci-worker'
                ,'node_b-ricci-modules'
                )
                ,Delegate\
                ('script.sh'
                ,'other-initscripts'
                ,lhead='cluster.node_b-initscripts'
                )
                ,Delegate\
                ('apache.sh'
                ,'httpd'
                ,lhead='cluster.node_b-installed-services'
                )
                ,Delegate\
                ('node_b-rgmanager'
                ,'ip.sh'
                ,lhead='cluster.node_b-resource-agents'
                )
                ,Delegate\
                ('fs.sh'
                ,'node_b-io'
                ,ltail='cluster.node_b-resources-aux'
                )
                ,Delegate\
                ('node_b-cman_tool'
                ,'ccs_sync'
                )
                ,Delegate\
                ('node_b-fenced'
                ,'node_b-fence_others'
                ,constraint='false'
                ,lhead='cluster.node_b-fence-agents'
                )
                ,Delegate\
                ('node_b-libdlm'
                ,'node_b-dev_dlm'
                )
                ,Databus\
                ('node_b-dlm'
                ,'node_b-dev_dlm'
                )
                ,MapsTo\
                ('node_b-configfs_dlm'
                ,'node_b-sys_kernel_config_dlm'
                )]
            )
            ,SubgraphImportant('cluster.node_a'
            #
            # node a
            #
            ,label='NODE A'
            ,_subgraphs=\
                [SubgraphStandard('cluster.node_a-conga'
                #
                # node a - management
                #
                ,label='management (incomplete)'
                ,_nodes=\
                    [CmanRicci('node_a-ricci'
                    )
                    ,CmanModclusterd('node_a-modclusterd'
                    )]
                )]
            ,_nodes=\
                [Program('node_a-kernel'
                ,label='kernel'
                )
                ,CmanRGManager('node_a-rgmanager'
                )]
            )]
        ,_edges=\
            ["modclusterd peer-to-peer network"][:0] +\
            [CmanModclusterdUpdates\
            ('node_a-modclusterd'
            ,'node_b-modclusterd'
            ,weight='1'
            )
            ,CmanModclusterdUpdates\
            ('node_a-modclusterd'
            ,'node_c-modclusterd'
            ,weight='1'
            )
            ,CmanModclusterdUpdates\
            ('node_b-modclusterd'
            ,'node_c-modclusterd'
            ,weight='1'
            )
            ,EdgeInvisible\
            ('node_a-modclusterd'
            ,'node_b-modclusterd'
            )
            ,EdgeInvisible\
            ('node_b-modclusterd'
            ,'node_c-modclusterd'
            )]
        )
        ,SubgraphStandard('cluster.services'
        #
        # cluster services layer
        #
        ,label='clustered services'
        ,_nodes=\
            [Program('apache-node_b'
            ,label='apache@B'
            )
            ,Program('mysql-node_c'
            ,label='mysql@C'
            )
            ,Program('other-clustered-services'
            ,label='…'
            )]
        )
        ,SubgraphStandard('cluster.networking'
        #
        # networking (storage and switch/es)
        #
        ,label='managed devices'
        ,_subgraphs=\
            [SubgraphStandard('cluster.switch'
                ,label='switch'
                ,_nodes=\
                    [StorageDevice('switch-fc'
                    ,label='FC'
                    )
                    ,StorageDevice('switch-ethernet'
                    ,label='Ethernet'
                    )]
            )]
        ,_nodes=\
            [StorageDevice('nas'
            ,label='NAS'
            )]
        )
        ,SubgraphStandard('cluster.fence'
        #
        # cluster devices layer
        #
        ,label='I/O fence facilities'
        ,_subgraphs=\
            [SubgraphStandard('cluster.direct-fence'
            #
            # direct fencing
            #
            ,label='direct fencing'
            ,_subgraphs=\
                [SubgraphStandard('cluster.hypervisor'
                #
                # hypervisor (in a role of power fence executor)
                #
                ,label='hypervisor as controller\n(when cluster virtualized)'
                ,rank='same'
                ,_nodes=\
                    [SharedFenceVirtd('fence_virtd'
                    )
                    ,Artefact('fence_virt.conf'
                    ,label='/etc/fence\n_virt.conf'
                    )
                    ,Daemon('libvirt'
                    )]
                ,_edges=\
                    [Delegate\
                    ('fence_virtd'
                    ,'libvirt'
                    ,constraint='false'
                    )
                    ,Consume\
                    ('fence_virtd'
                    ,'fence_virt.conf'
                    )]
                )]
            ,_nodes=\
                [FenceDevice('direct-fence'
                ,label='control card\n(drac,…)/power\ncontrol (apc,…)'
                )]
            )
            ,SubgraphStandard('cluster.switch-based-fence'
            #
            # switch-based fencing
            #
            ,label='switch-based'
            ,_nodes=\
                [FenceDevice('switch-based-fence'
                ,label='Ethernet (ifmib)/\nFibre Channel\n(brocade,…)/…'
                )]
            )]
        )]
    ,_edges=\
        [
        #
        # cluster nodes layer - cluster devices layer
        #

        # fence
        ]+\
        [Delegate\
        ('node_b-fence_virt'
        ,'fence_virtd'
        ,label='serial port/\nVMChannel\n/TCP (virt),\nmcast (xvm)'
        )
        ,Delegate\
        ('node_b-fence_others'
        ,'direct-fence'
        ,label='telnet,\nssh,\nsoap,\n…'
        ,ltail='cluster.node_b-fence-agents'
        ,lhead='cluster.fence'
        )
        ,Delegate\
        ('node_b-fence_scsi-sanlock'
        ,'nas'
        ,label='telnet,\nssh,\nsoap,\n…'
        )
        # shared storage
        ]+\
        [Databus\
        ('node_a-kernel'
        ,'switch-ethernet'
        ,lhead='cluster.networking'
        ,tailport='nw'
        )
        ,Databus\
        ('node_b-io'
        ,'switch-ethernet'
        ,lhead='cluster.networking'
        )
        ,Databus\
        ('node_c-kernel'
        ,'nas'
        ,lhead='cluster.networking'
        )
        # cluster devices layer - cluster nodes layer (fence execution)
        ]+\
        [FencedBy\
        ('node_a-kernel'
        ,'direct-fence'
        ,ltail='cluster.node_a'
        ,lhead='cluster.direct-fence'
        ,tailport='w'
        )
        ,FencedBy\
        ('node_b-io'
        ,'libvirt'
        ,ltail='cluster.node_b'
        ,lhead='cluster.direct-fence'
        ,constraint='false'
        )
        ,FencedBy\
        ('node_c-kernel'
        ,'libvirt'
        ,ltail='cluster.node_c'
        ,lhead='cluster.direct-fence'
        ,minlen='2'
        ,weight='2'
        )
        ,MapsTo\
        ('switch-based-fence'
        ,'switch-ethernet'
        ,ltail='cluster.switch-based-fence'
        ,lhead='cluster.switch'
        )
        ,FencedBy\
        ('nas'
        ,'switch-fc'
        ,constraint='false'
        )]
    )]
,_edges=\
    [
    #
    # cluster management layer - cluster nodes layer
    #
    ]+\
    [CmanRicciRPC\
    ('ccs'
    ,'node_a-ricci'
    ,ltail='cluster.management-cli'
    )
    ,CmanRicciRPC\
    ('luci'
    ,'node_b-ricci'
    ,ltail='cluster.management-cli'
    )
    ,CmanRicciRPC\
    ('ccs_sync'
    ,'node_c-ricci'
    ,ltail='cluster.management-cli'
    )
    #
    # outer environment - cluster management layer
    #
    ]+\
    [CmanLuciHTTPS\
    ('firefox'
    ,'luci'
    )
    ,DelegateSNMP\
    ('snmpwalk'
    ,'node_b-snmpd'
    )
    ,DelegateCIM\
    ('wbemcli'
    ,'node_b-cimserver'
    )
    ,Delegate\
    ('luci'
    ,'saslauthd'
    ,constraint='false'
    )
    # node b/rgmamanger - cluster services layer
    ]+\
    [Delegate\
    ('node_a-rgmanager'
    ,'apache-node_b'
    ,lhead='cluster.services'
    ,weight='2',
    )
    ,Delegate\
    ('node_b-rgmanager'
    ,'apache-node_b'
    ,lhead='cluster.services'
    ,weight='1',
    )
    ,Delegate\
    ('node_c-rgmanager'
    ,'apache-node_b'
    ,lhead='cluster.services'
    ,weight='1',
    )]
)


###


if __name__ == '__main__':
    main(graph, argv, *FONTDEF, blacklist=BLACKLIST, output=OUTPUT)
