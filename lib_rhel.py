#!/usr/bin/env python
# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
# Distributed under GPLv2+;  generated content under CC-BY-SA 3.0
# (to view a copy, visit http://creativecommons.org/licenses/by-sa/3.0/)
"""Library comprising general (non-cluster) RHEL entities"""

from lib_nodes import *


#
# nodes
#

class RhelSnmpd(Daemon):
    defaults = dict((
        LABEL('snmpd'),
    ))
    summary = \
    "Snmpd is daemon to respond to SNMP request packets"
    web = 'http://net-snmp.sourceforge.net/'
    man = [
        'snmpd(8)',
    ]
    #srpm = 'net-snmp'
    rpm = 'net-snmp'
    #rhbz = 'net-snmp'


class RhelCimserver(Daemon):
    defaults = dict((
        LABEL('cimserver'),
    ))
    summary = \
    "CIM Server"
    web = 'http://www.openpegasus.org/'
    man = [
        'cimserver(8)',
    ]
    #srpm = 'tog-pegasus'
    rpm = 'tog-pegasus'
    #rhbz = 'tog-pegasus'


class RhelSaslauthd(Daemon):
    defaults = dict((
        LABEL('saslauthd'),
    ))
    summary = \
    "sasl authentication server"
    web = 'http://asg.web.cmu.edu/sasl/sasl-library.html'
    man = [
        'saslauthd(8)',
    ]
    #srpm = 'cyrus-sasl'
    rpm = 'cyrus-sasl'
    #rhbz = 'cyrus-sasl'
