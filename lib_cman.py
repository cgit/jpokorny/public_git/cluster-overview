#!/usr/bin/env python
# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
# Distributed under GPLv2+;  generated content under CC-BY-SA 3.0
# (to view a copy, visit http://creativecommons.org/licenses/by-sa/3.0/)
"""Library comprising cman cluster entities and relationships"""

from lib_nodes import *
from lib_edges import *
from lib_shared import *


#
# edges/protocols
#

class CmanRicciRPC(ApplicationSpecificProtocol, Delegate):
    """[ricci client] -> ricci"""
    defaults = dict((
        LABEL('port 11111'),
    ))
    summary = 'ricci RPC is a customized variant of XMLRPC'
    web = 'http://www.sourceware.org/cluster/conga/ricci_api/index.html'
    #web = 'http://www.sourceware.org/cluster/conga/ricci_api/ricci_api.html'


class CmanRicciRPCSrc(object):
    common_src_of = CmanRicciRPC


class CmanLuciHTTPS(HTTPS, Delegate):
    """[web browser] -> luci"""
    defaults = dict((
        LABEL('port 8084'),
    ))
    secprops = dict(
        euser='luci',
        egroup='luci',
        # XXX: https://bugzilla.redhat.com/1023202
        #      https://bugzilla.redhat.com/1026374
        label='system_u:system_r:initrc_t:s0',
    )


class CmanModclusterdUpdates(Exchange):
    """modclusterd <-> modclusterd"""
    defaults = dict((
        LABEL('port 16851\n(either direction)'),
    ))
    summary = 'XML-formatted status messages'
    miscprops = dict(
        sockopts = ['O_NONBLOCK'
                   ,'TCP_NODELAY (since el6.3/https://bugzilla.redhat.com/742431#c10)'
                   ]
    )


class CmanModclusterdUpdatesPeer(object):
    common_src_of = CmanModclusterdUpdates
    common_dst_of = CmanModclusterdUpdates


#
# nodes
#

class CmanRGManager(Daemon):
    defaults = dict((
        LABEL('rgmanager'),
    ))
    summary = \
    "RGManager (Resource Group ~) deals with cluster resources"
    web = 'https://fedorahosted.org/cluster/wiki/RGManager'
    location = '/usr/sbin/rgmanager'
    repo = dict(
        git='git://git.fedorahosted.org/git/cluster.git',
        web='https://git.fedorahosted.org/cgit/cluster.git',
        ml='https://lists.fedorahosted.org/mailman/listinfo/cluster-commits',
    )
    man = [
        'rgmanager(8)',
    ]
    ids = dict(
        srpm = 'rgmanager',
        rpm = 'rgmanager',
        rhbz = 'rgmanager',
    )


class CmanClustat(Executable):
    defaults = dict((
        LABEL('clustat'),
    ))
    summary = \
    "clustat is a program to display cluster status"
    location = '/usr/sbin/clustat'
    repo = dict(
        git='git://git.fedorahosted.org/git/cluster.git',
        web='https://git.fedorahosted.org/cgit/cluster.git',
        ml='https://lists.fedorahosted.org/mailman/listinfo/cluster-commits',
    )
    man = [
        'clustat(8)'
    ]
    doc = {
        'Displaying HA Service Status with clustat'
        :'https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Cluster_Administration/s1-admin-manage-ha-services-cli-CA.html#s2-admin-manage-ha-services-clustat-cli-CA',
    }
    ids = dict(
        srpm='rgmanager',
        rpm='rgmanager',
        rhbz='rgmanager',
    )


class CmanCmanTool(Executable):
    defaults = dict((
        LABEL('cman_tool'),
    ))
    summary = \
    "cman_tool is a program to manage CMAN cluster subsystem"
    location = '/usr/sbin/cman_tool'
    repo = dict(
        git='git://git.fedorahosted.org/git/cluster.git',
        web='https://git.fedorahosted.org/cgit/cluster.git',
        ml='https://lists.fedorahosted.org/mailman/listinfo/cluster-commits',
    )
    man = [
        'cman_tool(8)',
    ]
    doc = {
        'Updating a Configuration Using cman_tool version -r'
        :'https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Cluster_Administration/s1-admin-updating-config-CA.html#s2-admin-prop-config-ccstoolr-cli-CA',
    }
    ids = dict(
        srpm='cluster',
        rpm='cman',
        rhbz='cluster',
    )


class CmanFenceTool(Executable):
    defaults = dict((
        LABEL('fence_tool'),
    ))
    summary = \
    "fence_tool is a utility for the fenced daemon"
    location = '/usr/sbin/fence_tool'
    repo = dict(
        git='git://git.fedorahosted.org/git/cluster.git',
        web='https://git.fedorahosted.org/cgit/cluster.git',
        ml='https://lists.fedorahosted.org/mailman/listinfo/cluster-commits',
    )
    man = [
        'fence_tool(8)',
    ]
    ids = dict(
        srpm='cluster',
        rpm='cman',
        rhbz='cluster',
    )


class CmanRicci(Daemon):
    defaults = dict((
        LABEL('ricci'),
    ))
    summary = \
    "ricci daemon is an executive back-end within cluster management (conga)"
    #web = 'https://sourceware.org/cluster/conga/'
    location = '/usr/sbin/ricci'
    repo = dict(
        git='git://git.fedorahosted.org/git/conga.git',
        web='https://git.fedorahosted.org/cgit/conga.git',
    )
    man = [
        'ricci(8)',
    ]
    ids = dict(
        srpm='ricci',
        rpm='ricci',
        rhbz='ricci',
    )
    secprops = dict(
        euser='ricci',
        egroup='root',
        label='unconfined_u:system_r:ricci_t:s0',
        capabilities='CAP_SYS_BOOT (+ temporarily CAP_SETUID)'
    )
    miscprops = dict(
        common_thread_cnt=1,
        nice=-1,
        ppid=1,
    )


class CmanRicciWorker(Program):
    defaults = dict((
        LABEL('ricci-worker'),
    ))
    summary = \
    "ricci-worker is a ricci's slave to actually handle the task batches"
    #web = 'https://sourceware.org/cluster/conga/'
    location = '/usr/libexec/ricci/ricci-worker'
    repo = dict(
        git='git://git.fedorahosted.org/git/conga.git',
        web='https://git.fedorahosted.org/cgit/conga.git',
    )
    ids = dict(
        srpm='ricci',
        rpm='ricci',
        rhbz='ricci',
    )
    secprops = dict(
        euser='ricci',
        egroup='root',
        #label='unconfined_u:system_r:ricci_t:s0',
    )


class CmanCcs(Executable, CmanRicciRPCSrc):
    defaults = dict((
        LABEL('ccs'),
    ))
    summary = \
    "ccs is a CLI front-end within cluster management (conga)"
    location = '/usr/sbin/ccs'
    repo = dict(
        #git='git://git.fedorahosted.org/git/conga.git',
        git='git://github.com/feist/ccs.git',
        #web='https://git.fedorahosted.org/cgit/conga.git',
        web='https://github.com/feist/ccs',
    )
    man = [
        'ccs(8)',
    ]
    doc = {
        'Configuring Red Hat High Availability Add-On With the ccs Command'
        :'https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Cluster_Administration/ch-config-ccs-CA.html',
        'Managing Red Hat High Availability Add-On With ccs'
        :'https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Cluster_Administration/ch-mgmt-ccs-CA.html',
    }
    ids = dict(
        srpm='ricci',
        rpm='ccs',
        rhbz='ricci',
    )


class CmanCcsSync(Executable, CmanRicciRPCSrc):
    defaults = dict((
        LABEL('ccs_sync'),
    ))
    summary = \
    "ccs_sync is a program to distribute+propagate cluster configuration"
    location = '/usr/bin/ccs_sync'
    repo = dict(
        git='git://git.fedorahosted.org/git/conga.git',
        web='https://git.fedorahosted.org/cgit/conga.git',
    )
    man = [
        'ccs_sync(8)',
    ]
    ids = dict(
        srpm='ricci',
        rpm='ricci',
        rhbz='ricci',
    )


class CmanLuci(Daemon, CmanRicciRPCSrc, HTTPSDst):
    defaults = dict((
        LABEL('luci'),
    ))
    summary = \
    "luci is a web front-end within cluster management (conga)"
    #web = 'https://sourceware.org/cluster/conga/'
    web = 'https://fedorahosted.org/cluster/wiki/Luci'
    repo = dict(
        git='git://git.fedorahosted.org/git/luci.git',
        web='https://git.fedorahosted.org/cgit/luci.git',
        ml='https://lists.fedorahosted.org/mailman/listinfo/luci-commits',
    )
    ids = dict(
        srpm='luci',
        rpm='luci',
        rhbz='luci',
    )
    doc = {
        'Configuring Red Hat High Availability Add-On With Conga'
        :'https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Cluster_Administration/ch-config-conga-CA.html',
        'Managing Red Hat High Availability Add-On With Conga'
        :'https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Cluster_Administration/ch-mgmt-conga-CA.html',
    }
    secprops = dict(
        euser='luci',
        egroup='luci',
        label='system_u:system_r:initrc_t:s0',
    )
    miscprops = dict(
        common_thread_cnt=11,  # can be really variable
        nice=10,
        ppid=1,
    )


class CmanModclusterd(Daemon, CmanModclusterdUpdatesPeer):
    defaults = dict((
        LABEL('modclusterd'),
    ))
    summary = \
    "modclusterd is a deamon facilitating info about cluster status (to conga)"
    location = '/usr/sbin/modclusterd'
    repo = dict(
        git='git://git.fedorahosted.org/git/conga.git',
        web='https://git.fedorahosted.org/cgit/conga.git',
    )
    ids = dict(
        srpm='clustermon',
        rpm='modcluster',
        rhbz='clustermon',
    )
    secprops = dict(
        euser='root',
        egroup='root',
        label='system_u:system_r:ricci_modclusterd_t:s0',
    )
    miscprops = dict(
        common_thread_cnt=3,
        nice=-1,
        ppid=1,
    )



class CmanQdiskd(Daemon):
    defaults = dict((
        LABEL('qdiskd'),
    ))
    summary = \
    "qdiskd is the cluster quorum disk daemon"
    repo = dict(
        git='git://git.fedorahosted.org/git/cluster.git',
        web='https://git.fedorahosted.org/cgit/cluster.git',
        ml='https://lists.fedorahosted.org/mailman/listinfo/cluster-commits',
    )
    man = [
        'qdiskd(8)',
    ]
    ids = dict(
        srpm='cluster',
        rpm='cman',
        rhbz='cluster',
    )


class  CmanModcluster(Executable):
    defaults = dict((
        LABEL('modcluster'),
    ))
    summary = \
    "modcluster is a ricci's helper dealing with cluster config and status"
    location = '/usr/libexec/modcluster'
    repo = dict(
        git='git://git.fedorahosted.org/git/conga.git',
        web='https://git.fedorahosted.org/cgit/conga.git',
    )
    ids = dict(
        srpm='clustermon',
        rpm='modcluster',
        rhbz='clustermon',
    )
    secprops = dict(
        euser='',
        egroup='',
        label='',
    )


class CmanFenced(Daemon):
    defaults = dict((
        LABEL('fenced'),
    ))
    summary = \
    "fenced is an I/O fencing daemon"
    location = '/usr/sbin/fenced'
    repo = dict(
        git='git://git.fedorahosted.org/git/cluster.git',
        web='https://git.fedorahosted.org/cgit/cluster.git',
        ml='https://lists.fedorahosted.org/mailman/listinfo/cluster-commits',
    )
    man = [
        'fenced(8)',
    ]
    ids = dict(
        srpm='cluster',
        rpm='cman',
        rhbz='cman',
    )
    secprops = dict(
        euser='root',
        egroup='root',
        label='system_u:system_r:fenced_t:s0',
    )
    miscprops = dict(
        common_thread_cnt=3,  # ?
        nice=0,
        ppid=1,
    )
