# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
# Distributed under GPLv2+;  generated content under CC-BY-SA 3.0
# (to view a copy, visit http://creativecommons.org/licenses/by-sa/3.0/)
"""Node library incl. visual aspects"""

from lib import LibNode
from lib_attributes import *


class NodeInvisible(LibNode):
    defaults = dict((
        STYLE.invis,
    ))


class Program(LibNode):
    defaults = dict((
        SHAPE.box,
        STYLE.filled,
    ))


class Agent(Program):
    defaults = dict((
        FILLCOLOR.lavenderblush,
        STYLE.filled,
    ))


class Library(Program):
    defaults = dict((
        FILLCOLOR.lavenderblush,
        STYLE.filled,
    ))


class Executable(Program):
    pass


class Daemon(Executable):
    defaults = dict((
        FILLCOLOR.cornsilk,
        STYLE.filled,
    ))


class UnixSocket(LibNode):
    defaults = dict((
        SHAPE.box3d,
        FILLCOLOR.wheat,
        STYLE.filled,
    ))


class Artefact(LibNode):
    defaults = dict((
        SHAPE.box3d,
        FILLCOLOR.wheat,
        STYLE.filled,
    ))


class Device(LibNode):
    defaults = dict((
        SHAPE.box3d,
        FILLCOLOR.wheat,
        STYLE.filled,
    ))


class StorageDevice(Device):
    pass


class FenceDevice(Device):
    pass
