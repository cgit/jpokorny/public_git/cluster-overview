# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
# Distributed under GPLv2+;  generated content under CC-BY-SA 3.0
# (to view a copy, visit http://creativecommons.org/licenses/by-sa/3.0/)
"""Attribute library incl. visual aspects"""

# XXX: perhaps validators, unificators (symbolic colors), ...

from lib import LibAttribute

g = globals()
for i in (
    'ARROWHEAD',
    'COLOR',
    'CONSTRAINT',
    'DIR',
    'FILLCOLOR',
    'FONTCOLOR',
    'LABEL',
    'SHAPE',
    'STYLE'):
    g[i] = type(i, (LibAttribute,), {})
