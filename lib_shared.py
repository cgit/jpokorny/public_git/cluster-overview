#!/usr/bin/env python
# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
# Distributed under GPLv2+;  generated content under CC-BY-SA 3.0
# (to view a copy, visit http://creativecommons.org/licenses/by-sa/3.0/)
"""Library comprising shared cluster entities and relationships"""

from lib_nodes import *
from lib_edges import *


class SharedFenceVirtd(Daemon):
    defaults = dict((
        LABEL('fence_virtd'),
    ))
    summary = \
    "fence_virtd is daemon listening for fence_virt's fencing requests"
    location = '/usr/sbin/fence_virtd'
    repo = dict(
        git='git://github.com/ryan-mccabe/fence-virt.git',
        #git='git://git.code.sf.net/p/fence-virt/code',
        web='https://github.com/ryan-mccabe/fence-virt',
        # web='http://sourceforge.net/p/fence-virt/code',
    )
    man = [
        'fence_virtd(8)'
    ]
    ids = dict(
        srpm='fence-virt',
        rpm='fence-virtd',  # + ~-{checkpoint, libvirt, multicast, serial}
        rhbz='fence-virt',
    )


class SharedFenceVirt(Executable):
    defaults = dict((
        LABEL('fence_virt'),
    ))
    summary = \
    "fence_{virt,xvm} is a program to request fencing via fence_virtd"
    location = '/usr/sbin/fence_virt'
    repo = dict(
        git='git://github.com/ryan-mccabe/fence-virt.git',
        #git='git://git.code.sf.net/p/fence-virt/code',
        web='https://github.com/ryan-mccabe/fence-virt',
        # web='http://sourceforge.net/p/fence-virt/code',
    )
    man = [
        'fence_virt(8)',
        'fence_xvm(8)',
    ]
    ids = dict(
        srpm='fence-virt',
        rpm='fence-virt',
        rhbz='fence-virt',
    )


class SharedCorosync(Daemon):
    defaults = dict((
        LABEL('corosync'),
    ))
    summary = \
    "Corosync is a Group Communication System with additional features" \
    + " for implementing high availability within applications"
    web = 'http://corosync.github.io/corosync/'
    location = '/usr/sbin/corosync'
    repo = dict(
        git='git://github.com/corosync/corosync.git',
        web='https://github.com/corosync/corosync',
        ml='http://lists.corosync.org/mailman/listinfo/discuss',
    )
    man = [
        'corosync(8)',
    ]
    ids = dict(
        srpm = 'corosync',
        rpm = 'corosync',
        rhbz = 'corosync',
    )
    secprops = dict(
        euser='root',
        egroup='root',
        label='unconfined_u:system_r:cluster_t:s0',
    )
    miscprops = dict(
        common_thread_cnt=8,  # ?
        ppid=1,
        sched_class='RR',
    )
