# vim: set fileencoding=UTF-8:
# Copyright 2013 Red Hat, Inc.
# Author: Jan Pokorný <jpokorny at redhat dot com>
# Distributed under GPLv2+;  generated content under CC-BY-SA 3.0
# (to view a copy, visit http://creativecommons.org/licenses/by-sa/3.0/)
"""Edge library incl. visual aspects"""

from lib import LibEdge
from lib_attributes import *


class EdgeInvisible(LibEdge):
    defaults = dict((
        STYLE.invis,
        ARROWHEAD.none,
    ))


class DBUS(LibEdge):
    defaults = dict((
        STYLE.dotted,
    ))


class OddjobExec(DBUS):
    defaults = dict((
        LABEL('oddjob\nexec'),
    ))


class HTTPS(LibEdge):
    defaults = dict((
        STYLE.bold,
    ))
    summary = 'HTTP over SSL/TLS'


class HTTPSDst(LibEdge):
    common_dst_of = HTTPS


class ApplicationSpecificProtocol(LibEdge):
    defaults = dict((
        STYLE.dashed,
    ))


class SNMP(ApplicationSpecificProtocol):
    defaults = dict((
        LABEL('port 161'),
    ))


class CIM(ApplicationSpecificProtocol):
    defaults = dict((
        LABEL('port 898[89]'),
    ))


class Consume(LibEdge):
    defaults = dict((
        COLOR.darkgreen,
        FONTCOLOR.darkgreen,
    ))


class ConsumeReversed(Consume):
    defaults = dict((
        DIR.back,
    ))


class Produce(LibEdge):
    defaults = dict((
        COLOR.orangered,
        FONTCOLOR.orangered,
    ))


class Delegate(LibEdge):
    defaults = dict((
        COLOR.navy,
        FONTCOLOR.navy,
        #DIR.both,
    ))


class MapsTo(LibEdge):
    defaults = dict((
        COLOR.tomato,
        FONTCOLOR.tomato,
        STYLE.dotted,
        CONSTRAINT.false,
    ))


class FencedBy(LibEdge):
    defaults = dict((
        COLOR.red,
        DIR.back,
    ))


# peer-to-peer (1:1 hence constraint='false')
class Exchange(LibEdge):
    defaults = dict((
        COLOR.saddlebrown,
        FONTCOLOR.saddlebrown,
        DIR.both,
        CONSTRAINT.false,
    ))


# data bus
class Databus(Exchange):
    defaults = dict((
        COLOR.saddlebrown,
        FONTCOLOR.saddlebrown,
    ))


# mixing

class DelegateOddjobExec(Delegate, OddjobExec):
    pass



class DelegateSNMP(Delegate, SNMP):
    pass


class DelegateCIM(Delegate, CIM):
    pass
